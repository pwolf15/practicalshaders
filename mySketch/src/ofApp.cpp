#include "ofApp.h"

//--------------------------------------------------------------
void ofApp::setup(){

	quad.addVertex(glm::vec3(-1.0f, -1.0f, 0.0f));
	quad.addVertex(glm::vec3(-1.0f, 1.0f, 0.0f));
	quad.addVertex(glm::vec3(1.0f, 1.0f, 0.0f));
	quad.addVertex(glm::vec3(1.0f, -1.0f, 0.0f));

	quad.addColor(ofDefaultColorType(1, 0, 0, 1));
	quad.addColor(ofDefaultColorType(0, 1, 0, 1));
	quad.addColor(ofDefaultColorType(0, 0, 1, 1));
	quad.addColor(ofDefaultColorType(1, 1, 1, 1));

	ofIndexType indices[6] = { 0, 1, 2, 2, 3, 0 };
	quad.addIndices(indices, 6);


	// uv coordinate assignment
	quad.addTexCoord(glm::vec2(0, 0));
	quad.addTexCoord(glm::vec2(0, 1));
	quad.addTexCoord(glm::vec2(1, 1));
	quad.addTexCoord(glm::vec2(1, 0));

	shader.load("scrolling_uv.vert", "mix_textures.frag");

	unifColor = glm::vec4(0.0f, 0.0f, 0.0f, 1.0f);

	ofDisableArbTex();
	imgParrot.load("parrot.png");
	imgChecker.load("checker.jpg");
	imgParrot.getTexture().setTextureWrap(GL_REPEAT, GL_REPEAT);
	imgChecker.getTexture().setTextureWrap(GL_REPEAT, GL_REPEAT);

	// brightness of texture
	brightness = 0.5f;

	ofDisableAlphaBlending();
}

//--------------------------------------------------------------
void ofApp::update(){
}

//--------------------------------------------------------------
void ofApp::draw(){
	shader.begin();
	shader.setUniformTexture("parrotTex", imgParrot, 0);
	shader.setUniformTexture("checkboardTex", imgChecker, 1);

	// shader.setUniform4f("fragCol", unifColor);
	shader.setUniform1f("time", ofGetElapsedTimef());
	shader.setUniform1f("brightness", brightness);
	shader.setUniform4f("multiply", glm::vec4(1.25, 1.25, 1.25, 1.25));
	shader.setUniform4f("add", glm::vec4(0.0, 0.0, 0.0, 0.0));
	quad.draw();
	shader.end();
}

//--------------------------------------------------------------
void ofApp::keyPressed(int key){
	//glm::vec3 curPos = triangle.getVertex(2);
	//triangle.setVertex(2, curPos + glm::vec3(-20, 0, 0));

	// unifColor = unifColor + glm::vec4(0.1f, 0.1, 0.1f, 0.0f);
}

//--------------------------------------------------------------
void ofApp::keyReleased(int key){

}

//--------------------------------------------------------------
void ofApp::mouseMoved(int x, int y ){

}

//--------------------------------------------------------------
void ofApp::mouseDragged(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mousePressed(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mouseReleased(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mouseEntered(int x, int y){

}

//--------------------------------------------------------------
void ofApp::mouseExited(int x, int y){

}

//--------------------------------------------------------------
void ofApp::windowResized(int w, int h){

}

//--------------------------------------------------------------
void ofApp::gotMessage(ofMessage msg){

}

//--------------------------------------------------------------
void ofApp::dragEvent(ofDragInfo dragInfo){ 

}
